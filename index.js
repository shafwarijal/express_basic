import express from "express";
// import Db from "./Database/db";
import fs from "fs";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/valorant/:query", (req, res) => {
  try {
    const data = JSON.parse(fs.readFileSync("./Database/db.json"));

    console.log(data.agents);
    const requestedQuery = req.params.query.toLowerCase();

    const agentsMatchingName = data.agents.filter(
      (agent) => agent.name.toLowerCase() === requestedQuery
    );

    const agentsMatchingRole = data.agents.filter(
      (agent) => agent.role.toLowerCase() === requestedQuery
    );

    const matchedAgents = [...agentsMatchingName, ...agentsMatchingRole];

    if (matchedAgents.length > 0) {
      res.json({
        agents: matchedAgents,
      });
    } else {
      res.status(404).json({ error: "No agents found" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
    console.log(error);
  }
});

app.post("/valorant", (req, res) => {
  const dataPath = "./Database/db.json"; // Path to the JSON file
  const newData = req.body; // Assuming your request body contains the new agent data

  try {
    const jsonData = JSON.parse(fs.readFileSync(dataPath));

    jsonData.agents.push(newData);
    console.log(newData);

    fs.writeFileSync(dataPath, JSON.stringify(jsonData, null, 2));

    res.status(201).json({ message: "Agent added successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
    console.log(error);
  }
});

app.put("/valorant/:name", (req, res) => {
  try {
    const dataPath = "./Database/db.json";
    const newName = req.params.name;
    const updatedData = req.body; // Updated data sent in the request body

    const jsonData = JSON.parse(fs.readFileSync(dataPath));

    const agentIndex = jsonData.agents.findIndex(
      (agent) => agent.name.toLowerCase() === newName.toLowerCase()
    );

    if (agentIndex === -1) {
      res.status(404).json({ error: "Agent not found" });
      return;
    }

    jsonData.agents[agentIndex] = {
      ...jsonData.agents[agentIndex],
      ...updatedData,
    };

    fs.writeFileSync(dataPath, JSON.stringify(jsonData, null, 2));

    res.json({ message: "Agent updated successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
});

app.delete("/valorant/:name", async (req, res) => {
  try {
    const dataPath = "./Database/db.json";
    const name = req.params.name;

    const jsonData = JSON.parse(fs.readFileSync(dataPath));
    const agentIndex = jsonData.agents.findIndex(
      (agent) => agent.name.toLowerCase() === name.toLowerCase()
    );

    if (agentIndex === -1) {
      res.status(404).json({ error: "Agent not found" });
      return;
    }

    jsonData.agents.splice(agentIndex, 1);
    fs.writeFileSync(dataPath, JSON.stringify(jsonData, null, 2));
    res.json({ message: "Agent deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
