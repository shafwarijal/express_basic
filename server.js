import express from "express";
import fs from "fs";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const port = 3000;

app.get("/", (req, res) => {
  res.send("Hello World!");
});

const dataPath = "./Database/db.json";

// JSON data agents
const data = JSON.parse(fs.readFileSync(dataPath));

// Func untuk simpan data
const saveData = (data) => {
  fs.writeFileSync(dataPath, JSON.stringify(data, null, 2));
};

// Memanggil semua data
app.get("/valorant", (req, res) => {
  try {
    res.send(data.agents);
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
});

// Memanggil data berdasarkan nama atau role
app.get("/valorant/:query", (req, res) => {
  try {
    // Mendapatkan params
    const requestedQuery = req.params.query.toLowerCase();

    // Filter data dari params
    const agentsByName = data.agents.filter(
      (agent) => agent.name.toLowerCase() === requestedQuery
    );

    const agentsByRole = data.agents.filter(
      (agent) => agent.role.toLowerCase() === requestedQuery
    );

    const agentsFilter = [...agentsByName, ...agentsByRole];
    if (agentsFilter.length > 0) {
      res.json(agentsFilter);
    } else {
      res.status(404).json({ error: "No agents found" });
    }
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
    console.log(error);
  }
});

// Menambahkan data ke dalam db.json
app.post("/valorant", (req, res) => {
  const newData = req.body;

  const existingAgent = data.agents.find(
    (agent) => agent.name.toLowerCase() === newData.name.toLowerCase()
  );
  if (existingAgent) {
    res.status(400).json({ error: "Agent with the same name already exists" });
    return;
  }

  try {
    data.agents.push(newData);
    saveData(data);
    res.status(201).json({ message: "Agent added successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
    console.log(error);
  }
});

// Mengubah data
app.put("/valorant/:name", (req, res) => {
  try {
    const newName = req.params.name;
    const updatedData = req.body;

    const agentIndex = data.agents.findIndex(
      (agent) => agent.name.toLowerCase() === newName.toLowerCase()
    );

    if (agentIndex === -1) {
      res.status(404).json({ error: "Agent not found" });
      return;
    }

    const existingAgent = data.agents.find(
      (agent) => agent.name.toLowerCase() === updatedData.name.toLowerCase()
    );

    if (existingAgent) {
      res
        .status(400)
        .json({ error: "Agent with the same name already exists" });
      return;
    }

    data.agents[agentIndex] = {
      ...data.agents[agentIndex],
      ...updatedData,
    };

    saveData(data);

    res.json({ message: "Agent updated successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
    console.log(error);
  }
});

// Mengahapus data
app.delete("/valorant/:name", async (req, res) => {
  try {
    const name = req.params.name;

    const agentIndex = data.agents.findIndex(
      (agent) => agent.name.toLowerCase() === name.toLowerCase()
    );

    if (agentIndex === -1) {
      res.status(404).json({ error: "Agent not found" });
      return;
    }

    data.agents.splice(agentIndex, 1);

    saveData(data);

    res.json({ message: "Agent deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: "Internal server error" });
  }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
